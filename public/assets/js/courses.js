//what will be the first task in retrieving all records inside the course collection
let adminControls = document.querySelector('#adminButton')
//target the container from the html document.
let container = document.querySelector('#courseContainer')

//lets determine if there is a user currently logged in 
//lets capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem("isAdmin")
//lets create a control structure to determine the display in the front end
if (isAdmin == "false" || !isAdmin) 
{
   adminControls.innerHTML = null;  
} else {
   adminControls.innerHTML = `
     <div class="col-md-2 offset-md-10 my-3">
      <a href="#" class="btn btn-block btn-warning">Add Course</a>
     </div>
   `
}

//send a request to retrive all documents from the courses collection 
fetch('http://localhost:5500/api/courses/Courses').then(res => res.json()).then(jsonData => {
  console.log(jsonData) //we only inserted this as a checker.

  
    let courseData; 


    if (jsonData.length < 1) {
      console.log("No Courses Available")
        courseData = "No Courses Available"
        container.innerHTML = courseData
    } else {
   
      courseData = jsonData.map(course => {
     
         console.log(course._id)
         console.log(course.name)
         
         let cardFooter; 
       
         if (isAdmin == "false" || !isAdmin) {
            cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
             `
         }
         else {
            cardFooter =
           `
             <a href="" class="btn btn-warning text-white btn-block">Edit Course</a>
             <a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
           `
         }

         return(
              `
             <div class="col-md-6 my-3">
                <div class="card">
                  <div class="card-body">
                    <h3 class="card-title">Course Name: ${course.name}</h3>
                    <p class="card-text text-left">Price: ${course.price}</p>
                    <p class="card-text text-left">Desc: ${course.description}</p>
                    <p class="card-text text-left">Created On: ${course.createdOn}</p>
                  </div>
                  <div class="card-footer">
                    ${cardFooter}
                  </div>
                </div>
             </div>
         `
          )
      }).join("") 
         container.innerHTML = courseData; 
    }
})
