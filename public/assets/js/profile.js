let token = localStorage.getItem("token");

let container = document.querySelector("#profileContainer")

fetch("http://localhost:5500/api/users/details", {
	method: 'GET', 
	headers: {
		'Content-type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {

	container.innerHTML = `
		<div class="col-md-13">
			<section class="jumbotron my-5">
				<h3 class="text-center">Firstname: ${jsonData.firstName}</h3>
				<h3 class="text-center">Lastname: ${jsonData.lastName}</h3>
				<h3 class="text-center">Email: ${jsonData.email}</h3>
				<h3 class="text-center">Mobile Number:${jsonData.mobileNo}</h3>

				<table class="table">
					<tr>
						<th>Course ID: </th>
						<th>Enrolled On: </th>
						<th>Status: </th>
						<tbody></tbody>
					</tr>
				</table>

			</section>
		</div>
	`

})