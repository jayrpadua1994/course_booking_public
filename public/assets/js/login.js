// console.log("hello from js")

//LETS target our form component inside our document. 
let loginForm = document.querySelector('#loginUser'); 

//the login form will be used by the client to insert his/her account to be authenticated by the app. 
loginForm.addEventListener("submit", (pagpasok) => {
   pagpasok.preventDefault()

 
  let email = document.querySelector("#userEmail").value
  let pass = document.querySelector("#password").value   

   if (email == "" || pass == "")
    {
    	alert("please input your email and/or address first")
    } 
   else
    {

    	fetch("http://localhost:5500/api/users/login", {
    		method: 'POST',
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			email: email, 
    			password: pass
    		})
    	}).then(res => {
    		return res.json()
    	}).then(data => {


        if (data.accessToken) {
          localStorage.setItem('token',data.accessToken)

          fetch("http://localhost:5500/api/users/details", {      
            headers: {

              'Authorization': `Bearer ${data.accessToken}`
            }
          }).then(res => {
            return res.json()
          }).then(data1 => {
          // console.log(data1)
      localStorage.setItem("id", data1._id)
      localStorage.setItem("isAdmin", data1.isAdmin)
      // console.log("items are set inside the local storage")
      if(data1.isAdmin){
        window.location.replace('../pages/courses.html')
      }else{
        window.location.replace('../pages/profile.html')
      }

 })



        // 
        } else {
          alert("no access token generated")         
        }
        
         
          
				// Swal.fire({
				// position: 'center',
				// icon: 'success',
				// title: 'successfully generated access token!',
				// showConfirmButton: true,
				// timer: 1500
				// })
           	 
    	})
    }
}) 
