
let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit",(e) => {
	e.preventDefault()  //to avoid page refresh

//capture each values inside the input fields first, the reapakcge them in a new varialble
let firstName = document.querySelector("#firstName").value
let lastName = document.querySelector("#lastName").value
let email = document.querySelector("#userEmail").value
let mobileNo = document.querySelector("#mobileNumber").value
let password = document.querySelector("#password1").value
let verifyPassword = document.querySelector("#password2").value


//data validation for our register page
//validation to make sure that the storage space will be propertly utelize

// stretch: these are not required
// the password should contain both alphaneumeric characters and atleast 1 special character.
// find a way so that the mobile will be able to accept both foreign and local mobile number formats.

if(!checkEmpty(firstName,lastName,email,mobileNo,password,verifyPassword)){
Swal.fire("Pls fill out all the fields ")
}else if(!checkMobile(mobileNo)){
  Swal.fire("Your Phone Number should be 11 numbers!")
}else if(!checkPasswordLength(password)){
  Swal.fire("Your password should be 8-16 Characters!")
}else if(!validatePassword(password,verifyPassword)){
   Swal.fire("Password should be the same")
}else {

	fetch('http://localhost:5500/api/users/email-exists', {
   method: 'POST',
   headers: {
     'Content-Type': 'application/json'
   }, 
   body: JSON.stringify({
    email: email
  })
 }).then(res => {
          return res.json() //make it readable once the response returns to the client side. 
        }).then(convertedData => {
      
          //control structure to determine the proper procedure depending on the response 
          if (convertedData === false) { 
              fetch('http://localhost:5500/api/users/register', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  firstName: firstName,
                  lastName: lastName,
                  email: email,
                  mobileNo: mobileNo, 
                  password: password
                }) 
              }).then(res => {
                console.log(res)
                return res.json()
              }).then(data => {
                
                //control structure to give out a proper response depending on the return from the backend. 
                if(data === true){
                  Swal.fire({
                    icon: 'success',
                    title: 'registration',
                    text: 'Complete!',
                    footer: '<a href="../pages/login.html">Go to login page</a>'
                  })
                
               }else{
                   Swal.fire("Something with wrong during processing!")
                 }
               }
               )
            } else {
               Swal.fire("The Email Already exist!")

             }
           }
           )
    }
 }) 
 checkPasswordLength=(pass)=>{
  
if(pass.length >= 8 && pass.length <=16 ){
  return true;
}
return false;

}

checkMobile =(mobileNo)=>{
 if(mobileNo.length === 11){
  return true
 }
 return false;
}
validatePassword=(password, verifyPassword)=>{
  return (password === verifyPassword);
}
checkEmpty=(firstName,lastName,email,password,verifyPassword)=>{
  if((firstName !== "" && lastName!== "" && email !== "" && password !== "" && verifyPassword!== "" )){
    return true;
  }
  return false;
}


