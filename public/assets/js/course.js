
// URLSearchParams() > this medthod/constructor creates and returns a URLSearchparams object.

// window.location -> returns a location object with information aobut the "current" location of the document
//.search -> contains the query string section of the current URL
let urlValues =  new URLSearchParams(window.location.search)


let id = urlValues.get('courseId')
console.log(id) 

let token = localStorage.getItem('token')
console.log(token)

 
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")


fetch(`http://localhost:5500/api/courses/${id}`).then(data => {
data.json().then(data=>{
	name.innerHTML = data.name
	price.innerHTML = data.price
	desc.innerHTML = data.description	
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`
	document.querySelector("#enrollButton").addEventListener("click", () => {
		alert("Successfully enrolled")
})
	})
});
