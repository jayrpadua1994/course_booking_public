// create add course page

let formSubmit = document.querySelector('#createCourse')

//acquire an event that will applied in our form component
//create a sub function inside the method to describe the procedure that will take place upon triggering the event
formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

let name = document.querySelector("#courseName").value
let cost = document.querySelector("#coursePrice").value
let desc = document.querySelector("#courseDesc").value
	
	//let create a checker to see if we were able to capture the values of each input fields
// console.log(name)
// console.log(cost)
// console.log(desc)	

// send a request to the backend project to process the data for creating a new entry inside our courses collection


	//upon creating this fetch API request we are instantiating a promise
if(name !== "" && desc !== "" && cost !== ""){
	
	fetch('http://localhost:5500/api/course/course-exists', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			name: name
		})
	}).then(res => {
		console.log(res)
		return res.json()
	}).then(convertedData => {
		
		if (convertedData === false) {
			fetch('http://localhost:5500/api/course/' , {
		method: 'POST',
		headers: {
			'content-Type': 'application/json'
		},
		body: JSON.stringify({
			// what are the properties of the document that the user need to fill
			name: name ,
			description: desc,
			price: cost
		})
	}).then(res => {
// console.log(res)
		return res.json()
	}).then(info => {
// console.log(info)
	// create a control structure that will describe the response of the ui to the client

	if (info === true) {
		Swal.fire('Course successfully Created!')
	} else {
		Swal.fire('Something Went Wrong!')

		}
	})
		} else {
			Swal.fire("The Course Already Exists!")
		}
	})



	 //how can we handle the promise object that will be returned once the fetch method event has happend?
}else{
	Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Something went wrong! check input fields!',
  
})
}
})